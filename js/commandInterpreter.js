function initializeInterpreter() {
    document.querySelector('.commandInputField').onkeypress = function(e){
        if (!e) e = window.event;
        var keyCode = e.keyCode || e.which;
        if (keyCode == '13'){
            // Enter pressed
            console.log(this.value);
            return false;
        }
    }
}
