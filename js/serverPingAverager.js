/**
 * Created by Tyler on 10/8/2016.
 */
// TODO: Implement
const numPingsToStore = 20;
const updateIntervalMillis = 1000; // How often the server is pinged (in milliseconds)
const timeoutMillis = 4000; // How long are willing to wait without a response from the server before assuming server is not responding
var lastPingsToServerMillis = []; // Only used for averaging, cleared if server is not responding

function getAvgServerPing() {

    // Check for divide by zero
    if (lastPingsToServerMillis.length == 0) {
        return undefined;
    }

    var sum = 0;
    for (var i = 0; i < lastPingsToServerMillis.length; i++) {
        sum += lastPingsToServerMillis[i];
    }

    return sum / lastPingsToServerMillis.length;

}

function getAvgRobotPing() {
    /* TODO: return serverFunction();
        Find out how to call the server function that returns avg robot ping
     */
}

// Constantly run with an interval below the function
function getServerPing() {

    // Returns undefined if server does not respond by specified amount of time (timeoutMillis)
    setTimeout(function() { return undefined }, timeoutMillis);

    // Server ping that will be returned once server responds (if it does respond)
    var serverPingMillis;



}

// Updates lastPingsToServerMillis array with get server ping at an interval
setInterval(function() {
    var serverPing = getServerPing();

    if (serverPing == undefined) {
        // This variable is only used for averaging serverPing, if the server isn't responding, then we signify that with an empty array
        lastPingsToServerMillis = [];
    } else {
        // Keep the array at or below max length by deleting the first element and shifting all other values left
        if (lastPingsToServerMillis.length == numPingsToStore) {
            lastPingsToServerMillis.shift();
        }
        lastPingsToServerMillis.push(serverPing);
    }
}, updateInterval);