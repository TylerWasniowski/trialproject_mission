/**
 * Created by Tyler on 10/5/2016.
 */
// Replaces elements suffixed with "Placeholder" with their actual elements located in the "modules" dir
    // ***Correctly spelt names are very important!!!***
function loadModule(moduleLink) {

    var moduleName = getModuleName(moduleLink);

    var placeholders = document.querySelectorAll('.' + moduleName + 'Placeholder');

    if (placeholders.length < 1) {
        console.log("\tNo " + moduleName + " placeholders found.");
        return;
    }


    // Load sidebar content from sidebar document
    var moduleDocument = moduleLink.import;

    var moduleContent = moduleDocument.querySelector('.' + moduleName);

    if (moduleContent == null) {
        console.log("\t" + moduleName.charAt(0).toUpperCase() + moduleName.substring(1, moduleName.length) + " document found, but content of module is missing.");
        return;
    }

    // Replace sidebar placeholder with actual sidebar
    for (var i = 0; i < placeholders.length; i++) {
        var placeholder = placeholders[i];
        placeholder.parentNode.replaceChild(moduleContent.cloneNode(true), placeholder);
    }
    console.log("\t" + moduleName + " loaded");

}

function getModuleName(moduleLink) {

    var moduleLinkHref = moduleLink.getAttribute('href');
    var moduleName = "";
    for (var i = 8; i < moduleLinkHref.length; i++) {
        if (moduleLinkHref.charAt(i) == ".")
            return moduleName;

        moduleName += moduleLinkHref.charAt(i);
    }

    return moduleName;

}

function loadModules() {

    console.log("Loading modules: ");

    var modulesToLoad = document.querySelectorAll('head link[rel="import"][href^="modules/"]');

    for (var i = 0; i < modulesToLoad.length; i++) {
        loadModule(modulesToLoad[i]);
    }



}