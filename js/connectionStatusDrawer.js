/**
 * Created by Tyler on 10/5/2016.
 */

var updateFrequencyMillis = 100;
var intervals = []; // Used to periodically update connection status

function updateConnectionStatus(canvasName, pingMillis) {

    // Get canvas for drawing space
    var canvas = document.querySelector('.' + canvasName);
    // The drawing requires access to the rendering context
    var context = canvas.getContext("2d");

    /*
        Display a status in the form of a colored circle showing how healthy the connection between mission control and the robot
    */

    console.log(pingMillis);
    // Colors stored in order from healthiest connection to least healthy connection
    var connectionColors = [[0, [156, 245, 1]], // Green at 0%
                            [75, [167, 228, 4]], // Yellowish-green at 75ms
                            [125, [255, 222, 0]], // Yellow at 125ms
                            [300, [255, 162, 0]], // Light-orange at 300ms
                            [325, [231, 142, 63]], // Orange at 325ms
                            [1000, [255, 0, 0]]]; // Red at 1000ms

    if (pingMillis != undefined) {

        var pingColor = [];

        // Linear gradient
        for (var i = 1; i < connectionColors.length; i++) {
            if (pingMillis < connectionColors[i][0] || i == connectionColors.length - 1) {
                for (var j = 0; j < connectionColors[i][1].length; j++) {
                    // Find the slope of the line that represents the current color (indexed by j) that pingMillis falls in between
                    var lineSlope = (connectionColors[i][1][j] - connectionColors[i - 1][1][j]) / (connectionColors[i][0] - connectionColors[i - 1][0]);
                    // Solve for the current color value of the connection status and push it to pingColor
                    var colorValue = Math.round(lineSlope * (pingMillis - connectionColors[i][0]) + connectionColors[i][1][j]);
                    // Bound colorValue to be between 0 and 255
                    if (colorValue < 0) {
                        colorValue = 0;
                    } else if (colorValue > 255) {
                        colorValue = 255;
                    }

                    pingColor.push(colorValue);
                }
            }
        }

        context.fillStyle = 'rgb(' + pingColor[0] + ', ' + pingColor[1] + ', ' + pingColor[2] + ')';

    } else {
        context.fillStyle = 'rgb(153, 155, 163)';
    }

    // Draw a circle
    context.beginPath();
    context.arc(canvas.width / 2, canvas.height / 2, Math.min(canvas.width, canvas.height) / 2 - 1, 0, 2*Math.PI, false);
    context.fill();
    context.stroke();

}

function startUpdatingConnectionStatus(canvasName, pingFunction) {
    intervals.push(setInterval(function () {updateConnectionStatus(canvasName, pingFunction())}, updateFrequencyMillis));
}

function stopUpdatingConnectionStatus() {
    clearInterval(interval);
}